<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 04.09.2018
 * Time: 20:36
 */

abstract class AdminBase
{
    public static function checkAdmin(){
        $userId = User::checkLogged();

        $user = User::getUserById($userId);
        if($user['role']==='admin'){
            return true;
        }
        die('Access denied');
    }

}