<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 29.07.2018
 * Time: 20:18
 */

class Db
{

    public static function getConnection()
    {
        $paramPath = ROOT.'/config/db_params.php';
        $params = include ($paramPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn,$params['user'],$params['password']);

        return $db;

    }
}