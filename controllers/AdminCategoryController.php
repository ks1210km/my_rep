<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 04.09.2018
 * Time: 20:35
 */

class AdminCategoryController extends AdminBase
{
    public function actionIndex(){
        self::checkAdmin();

        $categoriesList = Category::getCategoriesListAdmin();
        require_once (ROOT.'/views/admin_category/index.php');
    }

    public function actionDelete($category, $delete, $id){
        self::checkAdmin();
        if(isset($_POST['submit'])) {
            Category::deleteCategoryById($id);
            header('Location: /admin/category/');
        }
        require_once(ROOT . '/views/admin_category/delete.php');
    }

    public function actionUpdate($category, $update, $id){

        self::checkAdmin();
        $category = Category::getCategoryById($id);
        $errors = false;

        if(isset($_POST['submit'])) {
            $options['id'] = $id;
            $options['name'] = $_POST['name'];
            $options['sort_order'] = $_POST['sort_order'];
            $options['status'] = $_POST['status'];


            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля!';
            }
            if($errors === false){
                $result = Category::updateCategoryById($options);
            }

            header("Location: /admin/category");
        }



        require_once (ROOT.'/views/admin_category/update.php');

    }

    public function actionCreate(){
        self::checkAdmin();
        $errors = false;

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['sort_order'] = $_POST['sort_order'];
            $options['status'] = $_POST['status'];


            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля!';
            }

            if ($errors === false) {
               Category::addCategory($options);

            header("Location: /admin/category");
            }
        }

        require_once (ROOT. '/views/admin_category/create.php');
    }
}