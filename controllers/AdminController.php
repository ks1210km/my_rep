<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 04.09.2018
 * Time: 20:32
 */

class AdminController extends AdminBase
{
    public function actionIndex(){
        self::checkAdmin();
        require_once (ROOT.'/views/admin/index.php');
    }

}