<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 04.09.2018
 * Time: 20:35
 */

class AdminOrderController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();
        $orderList = Order::getOrderList();

        require_once(ROOT . '/views/admin_order/index.php');
    }

    public function actionUpdate($order,$update,$id)
    {

        self::checkAdmin();
        $errors = false;
        $order = Order::getOrderById($id);
        if(isset($_POST['submit'])){
            $options['id'] = $id;
        $options['user_name'] = $_POST['user_name'];
        $options['user_phone'] = $_POST['user_phone'];
        $options['user_comment'] = $_POST['user_comment'];
        $options['user_id'] = $_POST['user_id'];
        $options['products'] = $_POST['products'];
        $options['status'] = $_POST['status'];
            if (!isset($options['user_name']) || empty($options['user_name'])) {
                $errors[] = "Заповніть поле Ім'я користувача!";
            }
            if (!isset($options['user_phone']) || empty($options['user_phone'])) {
                $errors[] = "Заповніть поле Номер телефону!";
            }

            if($errors === false){
                $result = Order::updateOrder($options);
            }

            header("Location: /admin/order");

        }

        require_once(ROOT . '/views/admin_order/update.php');
    }

    public function actionDelete($order, $delete, $id)
    {
        self::checkAdmin();
        if (isset($_POST['submit'])) {
            Order::deleteOrderById($id);
            header('Location: /admin/order/');
        }
        require_once(ROOT . '/views/admin_order/delete.php');
    }

    public function actionView($controller, $view, $id){
        self::checkAdmin();

        $order = Order::getOrderById($id);
        $productsQuantity = json_decode($order['products'], true);

        $productsIds = array_keys($productsQuantity);
        $products = Product::getProductsByIds($productsIds);


        require_once(ROOT.'/views/admin_order/view.php');
    }
}