<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 19.08.2018
 * Time: 16:38
 */

class CabinetController
{
    public function actionIndex(){

       $userId = User::checkLogged();

       $user = User::getUserById($userId);
        require_once (ROOT. '/views/cabinet/index.php');
    }


    public function actionEdit(){

        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $name = $user['name'];
        $password = $user['password'];
        $result = false;

       if(isset($_POST['submit'])){
           $name = $_POST['name'];
           $password = $_POST['password'];
           $errors = false;

           if (!User::checkPassword($password)) {

               $errors[] = 'Пароль повинен бути не менше 6-ти символів';
           }
           if (!User::checkName($name)) {

               $errors[] = "Ім'я повинно бути не менше 2-х символів";
           }
           if($errors === false){
               $result = User::edit($userId,$name,$password);
           }

       }
        require_once (ROOT. '/views/cabinet/edit.php');
    }

}