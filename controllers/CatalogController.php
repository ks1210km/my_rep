<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 02.08.2018
 * Time: 16:07
 */


class CatalogController
{
    public function actionIndex()
    {
        $categories = Category::getCategoriesList();
        $latestProducts = Product::getLatestProducts();


        require_once(ROOT . '/views/catalog/index.php');
    }

    public function actionCategory($categoryId, $page = 'page-1'){
        $categories = Category::getCategoriesList();

        $page = substr($page,5);
        $categoryProducts = Product::getProductListByCategory($categoryId, $page);


        $total = Product::getTotalProductsInCategory($categoryId);


       $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT,'page-');
        require_once(ROOT . '/views/catalog/category.php');
    }
}