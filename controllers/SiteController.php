<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 01.08.2018
 * Time: 17:54
 */

class SiteController
{
    public function actionIndex()
    {
        $categories = Category::getCategoriesList();
        $latestProducts = Product::getLatestProducts(6);
        $recommendedProducts = Product::getRecommendedProducts();
        require_once(ROOT.'/views/site/index.php');
    }

    public function actionContact(){
        $mail = 'ks1210km@gmail.com';
        $subject = 'Тема листа';
        $message = 'Зміст листа';

        $result = mail($mail,$subject,$message);

        vardump($result);

        die;
    }

    public function actionAbout(){

        require_once (ROOT.'/views/site/about.php');
    }


}