<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 22.08.2018
 * Time: 19:52
 */

class Cart
{
    public const  SHOW_BY_DEFAULT = 6;

    public static function addProduct($id)
    {
        $id = (int)$id;


        //ключ - ід товара, значення - кількість товара

        $productsInCart = array();

        if (isset($_SESSION['products'])) {
            $productsInCart = $_SESSION['products'];
        }

        if (array_key_exists($id, $productsInCart)) {
            $productsInCart[$id]++;
        } else {
            $productsInCart[$id] = 1;
        }

        $_SESSION['products'] = $productsInCart;

        return self::countItems();

    }

    public static function countItems()
    {
        if (isset($_SESSION['products'])) {
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                $count += $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }


    public static function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        }
        return false;
    }


    public static function getTotalPrice($products){

        $productsInCart = self::getProducts(); //ключ - ід товару, значення - кількість

        if($productsInCart){
            $total = 0;
            foreach ($products as $item){
                $total += $item['price']*$productsInCart[$item['id']];
            }
        }
        return $total;
    }

    public static function clear(){
if(isset($_SESSION['products'])){
    unset($_SESSION['products']);
}
    }
}