<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 01.08.2018
 * Time: 19:14
 */

class Category
{
    public static function getCategoriesList()
    {
        $db = Db::getConnection();

        $categoryList = array();

        $qu = 'SELECT id, name FROM category WHERE status = "1" ORDER BY sort_order, name ASC';
        $result = $db->prepare($qu);
        $result->execute();

        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $i++;
        }
        return $categoryList;
    }

    public static function getCategoriesListAdmin()
    {

        $db = Db::getConnection();

        $qu = 'SELECT id, name, sort_order, status FROM category ORDER BY sort_order ASC';
        $result = $db->prepare($qu);
        $result->execute();


        $categoryList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $categoryList[$i]['sort_order'] = $row['sort_order'];
            $categoryList[$i]['status'] = $row['status'];
            $i++;
        }
        return $categoryList;
    }

    public static function deleteCategoryById($id){
        $id = (int)$id;
        if ($id) {
            $db = DB::getConnection();

            $qu = 'DELETE FROM category WHERE id=:id';
            $result = $db->prepare($qu);
            $result->bindParam(':id',$id, PDO::PARAM_INT);
            $result->execute();
            return $result->fetch();
        }
    }

    public static function getCategoryById($id){
        $id = (int)$id;

        if($id){
            $db = Db::getConnection();
            $qu = 'SELECT * FROM category WHERE id = :id';
            $result = $db->prepare($qu);
            $result->bindParam(':id',$id,PDO::PARAM_INT);
            $result->execute();
            $result->setFetchMode(PDO::FETCH_ASSOC);
            return $result->fetch();
        }

    }

    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return 'Відображається';
                break;
            case '0':
                return 'Прихована';
                break;
        }
    }

    public static function updateCategoryById($options)
    {

        $db = Db::getConnection();

        $qu = "UPDATE category
            SET 
                name = :name, 
                sort_order = :sort_order, 
                status = :status
            WHERE id = :id";

        $result = $db->prepare($qu);
        $result->bindParam(':id', $options['id'], PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':sort_order', $options['sort_order'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function addCategory($options){

        $db = Db::getConnection();
        $qu = 'INSERT INTO category (name, sort_order, status) VALUES (:name, :sort_order, :status)';
        $result = $db->prepare($qu);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':sort_order', $options['sort_order'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getNumberOfCategories()
    {
        $db = DB::getConnection();

        $qu = 'SELECT count(id) AS count FROM category ';
        $result = $db->prepare($qu);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['count'];

    }
}