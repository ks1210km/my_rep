<?php

/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 29.07.2018
 * Time: 19:08
 */
class News
{
    public static function getNewsItemById($id)
    {
        $id = (int)$id;
        if ($id) {
            $db = Db::getConnection();
            $qu = 'SELECT * FROM news WHERE id=' . $id;
            $result = $db->query($qu);
            //$result->setFetchMode(PDO::FETCH_NUM);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $newsItem = $result->fetch();
            return $newsItem;
        }


    }

    public static function getNewsList()
    {
//        $host = 'localhost';
//        $dbname = 'mvc_site';
//        $user = 'root';
//        $password = '';
//        $db = new PDO("mysql:host=$host;dbname=$dbname,$user,$password");
        $db = Db::getConnection();

        $newsList = array();

        $qu = 'SELECT id,title,date,short_content,author_name FROM news ORDER BY date DESC LIMIT 10';
        $result = $db->prepare($qu);
        $result->execute();

        $i = 0;
        while ($row = $result->fetch()) {
            $newsList[$i]['id'] = $row['id'];
            $newsList[$i]['title'] = $row['title'];
            $newsList[$i]['date'] = $row['date'];
            $newsList[$i]['short_content'] = $row['short_content'];
            $newsList[$i]['author_name'] = $row['author_name'];
            $i++;
        }
        return $newsList;
    }
}