<?php

/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 01.08.2018
 * Time: 20:05
 */
class Product
{

    public const  SHOW_BY_DEFAULT = 3;

    public static function getProductById($id)
    {
        $id = (int)$id;
        if ($id) {
            $db = DB::getConnection();
            $product = array();

            $qu = 'SELECT * FROM product WHERE id=' . $id;
            $result = $db->prepare($qu);
            $result->execute();
            $result->setFetchMode(PDO::FETCH_ASSOC);
            return $result->fetch();
        }
    }
    public static function deleteProductById($id)
    {
        $id = (int)$id;
        if ($id) {
            $db = DB::getConnection();

            $qu = 'DELETE FROM product WHERE id=:id';
            $result = $db->prepare($qu);
            $result->bindParam(':id',$id, PDO::PARAM_INT);
            $result->execute();
            return $result->fetch();
        }
    }

    public static function getRecommendedProducts(){
        $products = array();

        $db = DB::getConnection();
        $qu = "SELECT * FROM product WHERE status='1' AND is_recommended='1'";
        $result = $db->prepare($qu);
        $result->execute();

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $products;
    }

    public static function getProductsByIds($idsArray)
    {
        $products = array();

        $db = DB::getConnection();
        $idsString = implode(',', $idsArray);
        $qu = "SELECT * FROM product WHERE status='1' AND id IN ($idsString)";
        $result = $db->prepare($qu);
        $result->execute();

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $i++;
        }
        return $products;

    }

    public static function getLatestProducts($count = self::SHOW_BY_DEFAULT)
    {
        $count = (int)$count;

        $db = DB::getConnection();

        $productsList = array();

        $qu = 'SELECT id, name, price, image, is_new FROM product WHERE status = "1" ORDER BY id DESC LIMIT ' . $count;
        $result = $db->prepare($qu);
        $result->execute();

        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['image'] = $row['image'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $productsList;
    }
    public static function getAllProducts()
    {

        $db = DB::getConnection();

        $productsList = array();

        $qu = 'SELECT id, name, code, price, status FROM product';
        $result = $db->prepare($qu);
        $result->execute();

        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['status'] = $row['status'];
            $i++;
        }
        return $productsList;
    }


    public static function getProductListByCategory($categoryId = false, $page = 1): array
    {
        if ($categoryId) {

            $page = (int)$page;
            $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

            $db = DB::getConnection();
            $products = array();

            $qu = 'SELECT id, name, price, image, is_new FROM product WHERE status = "1" AND category_id = "' . $categoryId . '" ORDER BY id ASC LIMIT ' . self::SHOW_BY_DEFAULT . ' OFFSET ' . $offset;
            $result = $db->prepare($qu);
            $result->execute();


            $i = 0;
            while ($row = $result->fetch()) {
                $products[$i]['id'] = $row['id'];
                $products[$i]['name'] = $row['name'];
                $products[$i]['price'] = $row['price'];
                $products[$i]['image'] = $row['image'];
                $products[$i]['is_new'] = $row['is_new'];
                $i++;
            }

            return $products;
        }
    }


    public static function getTotalProductsInCategory($categoryId)
    {
        $db = DB::getConnection();

        $qu = 'SELECT count(id) AS count FROM product WHERE status=1 AND category_id=' . $categoryId;
        $result = $db->prepare($qu);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];

    }


    public static function addProduct($options){
        $db = DB::getConnection();

        $qu = 'INSERT INTO product (name, category_id, code, price, availability, brand, description, is_new, is_recommended, status)
        VALUES (:name, :category_id, :code, :price, :availability, :brand, :description, :is_new, :is_recommended, :status)';
        $result= $db->prepare($qu);
        $result->bindParam(':name',$options['name'],PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_INT);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
           return $db->lastInsertId();
        }
        return 0;

    }
    public static function getImage($id){

        // Название изображения-пустышки
        $noImage = 'no-image.jpg';

        // Путь к папке с товарами
        $path = '/upload/images/products/';

        // Путь к изображению товара
        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            return $pathToProductImage;
        }

        // Возвращаем путь изображения-пустышки
        return $path . $noImage;

    }

    public static function updateProduct($id, $options){

        $db = Db::getConnection();

        $qu = "UPDATE product
            SET 
                name = :name, 
                code = :code, 
                price = :price, 
                category_id = :category_id, 
                brand = :brand, 
                availability = :availability, 
                description = :description, 
                is_new = :is_new, 
                is_recommended = :is_recommended, 
                status = :status
            WHERE id = :id";

        $result = $db->prepare($qu);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();

    }

}