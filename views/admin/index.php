<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <h4>Welcome, admin </h4>

            <br/>

            <p>Оберіть дію:</p>

            <br/>

            <ul>
                <li><a href="/admin/product">Керування товарами</a></li>
                <li><a href="/admin/category">Керування категоріями</a></li>
                <li><a href="/admin/order">Керування замовленнями</a></li>
            </ul>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

