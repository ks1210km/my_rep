<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li class="active">Керування замовленнями</li>
                </ol>
            </div>

            <a href="/admin/order/create" class="btn btn-default back"><i class="fa fa-plus"></i> Додати замовлення</a>

            <h4>Список замовлень</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID замовлення</th>
                    <th>Ім'я замовника</th>
                    <th>Номер телефону</th>
                    <th>Коментар</th>
                    <th>Дата</th>
                    <th>Статус замовлення</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($orderList as $order): ?>
                    <tr>
                        <td><?php echo $order['id']; ?></td>
                        <td><?php echo $order['user_name']; ?></td>
                        <td><?php echo $order['user_phone']; ?></td>
                        <td><?php echo $order['user_comment']; ?></td>

                        <td><?php echo $order['order_date']; ?></td>

<!--                        <!--БИДЛОКОД АХТУНГ-->
<!--                        <td>-->
<!--                            --><?php //$products = json_decode($order['products'],true);?>
<!--                            <button class="spoiler-title">Список товарів</button>-->
<!--                            <div class="spoiler-body">-->
<!--                            --><?php //foreach ($products as $id=>$count):?>
<!--                            --><?php //$product = Product::getProductById($id);
//                                echo 'id:'.$product['name'].'-'.$count.'шт<br/>';?>
<!--                            --><?php //endforeach;?>
<!--                            </div>-->
<!--                        </td>-->
                        <td><?php echo $order['status']; ?></td>

                        <td><a href="/admin/order/view/<?php echo $order['id']; ?>" title="Переглянути"><i class="fa fa-eye"></i></a></td>
                        <td><a href="/admin/order/update/<?php echo $order['id']; ?>" title="Редагувати"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/admin/order/delete/<?php echo $order['id']; ?>" title="Видалити"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

