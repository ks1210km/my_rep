<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 18.08.2018
 * Time: 20:01
 */

function my_autoload($class_name){
$array_paths = array(
    '/models/',
    '/components/');


foreach ($array_paths as $path){
    $path = ROOT . $path . $class_name .'.php';
    if(is_file($path)){
        include_once $path;
    }
}
}

spl_autoload_register('my_autoload');