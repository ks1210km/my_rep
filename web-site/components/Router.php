<?php


class Router
{


    private static $routes = array(
        array(
            'url' => '/catalog\/?/',
            'controller' => 'catalog',
            'action' => 'index'
        ),
//адмін-продукти
        array(
            'url' => '/admin\/product\/update\/[(0-9)+]\/?/',
            'controller' => 'adminProduct',
            'action' => 'update'
        ),array(
            'url' => '/admin\/product\/create\/?/',
            'controller' => 'adminProduct',
            'action' => 'create'
        ),array(
            'url' => '/admin\/product\/delete\/[(0-9)+]\/?/',
            'controller' => 'adminProduct',
            'action' => 'delete'
        ),array(
            'url' => '/admin\/product\/?/',
            'controller' => 'adminProduct',
            'action' => 'index'
        ),
  //адмін-категорія

        array(
            'url' => '/admin\/category\/update\/[(0-9)+]\/?/',
            'controller' => 'adminCategory',
            'action' => 'update'
        ),array(
            'url' => '/admin\/category\/delete\/[(0-9)+]\/?/',
            'controller' => 'adminCategory',
            'action' => 'delete'
        ),array(
            'url' => '/admin\/category\/create\/?/',
            'controller' => 'adminCategory',
            'action' => 'create'
        ), array(
            'url' => '/admin\/category\/?/',
            'controller' => 'adminCategory',
            'action' => 'index'
        ),


        array(
            'url' => '/admin\/order\/update\/[(0-9)+]\/?/',
            'controller' => 'adminOrder',
            'action' => 'update'
        ),array(
            'url' => '/admin\/order\/delete\/[(0-9)+]\/?/',
            'controller' => 'adminOrder',
            'action' => 'delete'
        ),array(
            'url' => '/admin\/order\/view\/[(0-9)+]\/?/',
            'controller' => 'adminOrder',
            'action' => 'view'
        ),array(
            'url' => '/admin\/order\/?/',
            'controller' => 'adminOrder',
            'action' => 'index'
        ),



        array(
            'url' => '/admin\/?/',
            'controller' => 'admin',
            'action' => 'index'
        ),


        array(
            'url' => '/cart\/([0-9]+)\/?/',
            'controller' => 'cart',
            'action' => 'addAjax'
        ),array(
            'url' => '/cart\/checkout\/?/',
            'controller' => 'cart',
            'action' => 'checkout'
        ),array(
            'url' => '/cart\/delete\/([0-9]+)?/',
            'controller' => 'cart',
            'action' => 'delete'
        ),array(
            'url' => '/cart\/?/',
            'controller' => 'cart',
            'action' => 'index'
        ),


        array(
            'url' => '/about\/?/',
            'controller' => 'site',
            'action' => 'about'
        ),

        array(
            'url' => '/cabinet\/edit\/?/',
            'controller' => 'cabinet',
            'action' => 'edit'
        ),array(
            'url' => '/cabinet\/?/',
            'controller' => 'cabinet',
            'action' => 'index'
        ),


        array(
            'url' => '/category\/([0-9]+)\/page-([0-9]+)/',
            'controller' => 'catalog',
            'action' => 'category'
        ),
        array(
            'url' => '/category\/([0-9]+)/',
            'controller' => 'catalog',
            'action' => 'category'
        ),


        array(
            'url' => '/product\/([0-9]+).*?/',
            'controller' => 'product',
            'action' => 'view'
        ),


        array(
            'url' => '/user\/login\/?/',
            'controller' => 'user',
            'action' => 'login'
        ), array(
            'url' => '/user\/logout\/?/',
            'controller' => 'user',
            'action' => 'logout'
        ), array(
            'url' => '/user\/register\/?/',
            'controller' => 'user',
            'action' => 'register'
        ),


        array(
            'url' => '//',
            'controller' => 'site',
            'action' => 'index'
        ),

    );

    //найти який з масівів в роутс підходить під адрес в браузері
    private static function getRoutes()
    {
        foreach (self::$routes as $value) {
            if (isset($_GET['route']))
                if (preg_match($value['url'], $_GET['route'])) {
                    return array(
                        'controller' => $value['controller'],
                        'action' => $value['action']
                    );
                }
        }

    }

    public function __construct()
    {

    }

//    private function getParams()
//    {
//        foreach (self::$routes as $value) {
//            return preg_replace($value['url'], '$1', $_GET['route']);
//
//        }
//
//    }

    public function run()
    {
        $segments = self::getRoutes();
        if ($segments !== NULL) {
            $controllerName = ucfirst(array_shift($segments) . 'Controller');
            $actionName = 'action' . ucfirst(array_shift($segments));
            $parameters = explode('/', $_GET['route']);
            array_shift($parameters);
            //var_dump('Class: (' . $controllerName . ') Method: (' . $actionName . ')');

            $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
            if (file_exists($controllerFile)) {
                include_once($controllerFile);
            }
            $controllerObject = new $controllerName;
            call_user_func_array(array($controllerObject, $actionName), $parameters);
            //  $controllerObject->$actionName($parameters);
        }

    }
}
