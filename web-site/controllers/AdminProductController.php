<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 04.09.2018
 * Time: 20:35
 */

class AdminProductController extends AdminBase
{
    public function actionIndex(){
        self::checkAdmin();

        $productsList = Product::getAllProducts();
        require_once (ROOT.'/views/admin_product/index.php');
    }

    public function actionDelete($product, $delete, $id){
        self::checkAdmin();
        if(isset($_POST['submit'])) {
            Product::deleteProductById($id);
            header('Location: /admin/product/');
        }
        require_once(ROOT . '/views/admin_product/delete.php');
    }

    public function actionUpdate($product, $update, $id){

        self::checkAdmin();
        $categoriesList = Category::getCategoriesListAdmin();
        $product = Product::getProductById($id);
        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];
            $options['description'] = $_POST['description'];
            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];


            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля!';
            }

            if (Product::updateProduct($id,$options)) {
                if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
                }
            }

                header("Location: /admin/product");
            }



        require_once (ROOT.'/views/admin_product/update.php');

}
    public function actionCreate(){
        self::checkAdmin();
        $categoriesList = Category::getCategoriesListAdmin();
        $errors = false;
        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];
            $options['description'] = $_POST['description'];
            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];


            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля!';
            }

            if ($errors === false) {
                $id = Product::addProduct($options);
                if ($id) {
                    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                        move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
                    }
                }
               header("Location: /admin/product");
            }

        }

        require_once (ROOT. '/views/admin_product/create.php');
    }

}