<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 01.08.2018
 * Time: 18:27
 */

class ProductController
{
    public function actionView($productId)
    {
        $categories = Category::getCategoriesList();
        $product = Product::getProductById($productId);
        require_once(ROOT . '/views/product/view.php');
    }
}