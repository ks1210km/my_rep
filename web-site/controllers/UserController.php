<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 18.08.2018
 * Time: 20:15
 */

class UserController
{
    public function actionRegister()
    {

        $name = '';
        $email = '';
        $password = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $errors = false;
            if (!User::checkName($name)) {

                $errors[] = "Ім'я повинно бути не менше 2-х символів";
            }

            if (!User::checkEmail($email)) {

                $errors[] = "Хибний email";
            }

            if (!User::checkPassword($password)) {

                $errors[] = 'Пароль повинен бути не менше 6-ти символів';
            }

            if(User::checkEmailExists($email)){
                $errors[] = 'Такий email вже використовується';
            }
            if($errors === false){
            $result = User::register($name,$email,$password);
            }


        }
        require_once(ROOT . '/views/user/register.php');
    }

    public function actionLogin(){
        $password = '';
        $email = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $errors = false;

            if (!User::checkEmail($email)) {

                $errors[] = 'Хибний email';
            }

            if (!User::checkPassword($password)) {

                $errors[] = 'Пароль повинен бути не менше 6-ти символів';
            }

            $userId = User::checkUserData($email,$password);
            if($userId===false){
                $errors[] = 'Невірні дані для входу на сайт';
            } else {
                User::auth($userId);

                header('Location: /cabinet/');

            }

        }
        require_once(ROOT . '/views/user/login.php');
    }

    public function actionLogout(){
        session_start();
        unset($_SESSION['user']);
        header('Location: /');
    }
}