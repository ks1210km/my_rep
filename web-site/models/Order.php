<?php
/**
 * Created by PhpStorm.
 * User: Amanku
 * Date: 31.08.2018
 * Time: 20:33
 */

class Order
{
    public static function save($userName, $userPhone, $userComment, $userId, $products)
    {
    $db = DB::getConnection();
    $qu = 'INSERT INTO product_order (user_name, user_phone, user_comment, user_id, products) '.
        'VALUES (:user_name, :user_phone, :user_comment, :user_id, :products)';

    $products = json_encode($products);
    $result = $db->prepare($qu);
    $result->bindParam(':user_name',$userName, PDO::PARAM_STR);
    $result->bindParam(':user_phone',$userPhone, PDO::PARAM_STR);
    $result->bindParam(':user_comment',$userComment, PDO::PARAM_STR);
    $result->bindParam(':user_id',$userId, PDO::PARAM_INT);
    $result->bindParam(':products',$products, PDO::PARAM_STR);
    return $result->execute();
    }

    public static function getOrderList(){
        $db = DB::getConnection();
        $qu = 'SELECT * FROM product_order';
        $result = $db->prepare($qu);
        $result->execute();


        $orderList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $orderList[$i]['id'] = $row['id'];
            $orderList[$i]['user_name'] = $row['user_name'];
            $orderList[$i]['user_phone'] = $row['user_phone'];
            $orderList[$i]['user_comment'] = $row['user_comment'];
            $orderList[$i]['user_id'] = $row['user_id'];
            $orderList[$i]['order_date'] = $row['order_date'];
            $orderList[$i]['products'] = $row['products'];  //отримую json товарів
            $orderList[$i]['status'] = $row['status'];
            $i++;
        }
        return $orderList;
    }

    public static function deleteOrderById($id){
        $id = (int)$id;
        if($id){
            $db = DB::getConnection();

            $qu = 'DELETE FROM product_order WHERE id=:id';
            $result = $db->prepare($qu);
            $result->bindParam(':id',$id, PDO::PARAM_INT);
            $result->execute();
            return $result->fetch();
        }
    }


    public static function getOrderById($id){
        $id = (int)$id;
        if($id) {
            $db = DB::getConnection();
            $qu = 'SELECT * FROM product_order WHERE id=:id';
            $result = $db->prepare($qu);
            $result->bindParam(':id',$id, PDO::PARAM_INT);
            $result->execute();
            $result->setFetchMode(PDO::FETCH_ASSOC);
            return $result->fetch();
        }
    }

    public static function updateOrder($options){
        $db = Db::getConnection();

        echo '<pre>';
        print_r($options);

        echo '</pre>';

        $qu = "UPDATE product_order
            SET 
                user_name = :user_name, 
                user_phone = :user_phone, 
                user_comment = :user_comment, 
                user_id = :user_id,                
                products = :products, 
                status = :status 
                
            WHERE id =:id";

        $result = $db->prepare($qu);
        $result->bindParam(':id', $options['id'], PDO::PARAM_INT);
        $result->bindParam(':user_name', $options['user_name'], PDO::PARAM_STR);
        $result->bindParam(':user_phone',  $options['user_phone'], PDO::PARAM_STR);
        $result->bindParam(':user_comment',  $options['user_comment'], PDO::PARAM_STR);
        $result->bindParam(':user_id',  $options['user_id'], PDO::PARAM_INT);
        $result->bindParam(':products',  $options['products'], PDO::PARAM_STR);
        $result->bindParam(':status',  $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getStatusText($status){
        switch ($status){
            case '1':
                return 'Нове замовлення';
                break;
            case '2':
                return 'Обробляється';
                break;
            case '3':
                return 'Доставка';
                break;
            case '4':
                return 'Закритий';
                break;
        }

    }
}