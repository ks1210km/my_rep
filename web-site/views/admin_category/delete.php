<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/product">Керування категоріями</a></li>
                    <li class="active">Видалення категорії</li>
                </ol>
            </div>


            <h4>Видалення категорії #<?php echo $id; ?></h4>


            <p>Ви дійсно бажаєте видалити категорію?</p>

            <form method="post">
                <input type="submit" name="submit" value="Видалити" />
            </form>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

