<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/order">Керування замовленнями</a></li>
                    <li class="active">Редагування замовлення</li>
                </ol>
            </div>


            <h4>Редагування замовлення #<?php echo $id; ?></h4>

            <br/>

            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">

                        <p>Ім'я замовника</p>
                        <input type="text" name="user_name" placeholder="" value="<?php echo $order['user_name']; ?>">

                        <p>Номер телефону</p>
                        <input type="tel" name="user_phone" placeholder="" value="<?php echo $order['user_phone']; ?>">
                        <p>Коментар</p>
                        <input type="text" name="user_comment" placeholder="" value="<?php echo $order['user_comment']; ?>">
                        <p>Id користувача</p>
                        <input type="text" name="user_id" placeholder="" value="<?php echo $order['user_id']; ?>">
                        <p>Товари</p>
                        <textarea name="products" placeholder=""> <?=$order['products']; ?> </textarea>
                        <p>Статус</p>
                        <select name="status">
                            <option value="1" <?php if ($order['status'] === 1) echo ' selected="selected"'; ?>>Нове замовлення</option>
                            <option value="2" <?php if ($order['status'] === 2) echo ' selected="selected"'; ?>>Обробляється</option>
                            <option value="3" <?php if ($order['status'] === 3) echo ' selected="selected"'; ?>>Доставка</option>
                            <option value="4" <?php if ($order['status'] === 4) echo ' selected="selected"'; ?>>Закритий</option>
                        </select>

                        <br/><br/>

                        <input type="submit" name="submit" class="btn btn-default" value="Зберегти">

                        <br/><br/>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

