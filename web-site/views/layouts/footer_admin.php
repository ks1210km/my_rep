<div class="page-buffer"></div>
</div>

<footer id="footer" class="page-footer"><!--Footer-->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Копірайт © </p>
                <p class="pull-right">Тупа сайт</p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->



<script src="/template/js/jquery.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>
<!--<script src="/template/js/tcal.js" type="text/javascript"></script>-->

<!--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>-->
<!--<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
<script type="text/javascript" src="/template/slick/slick.min.js"></script>


<!-- include Cycle2 -->

<script src="/template/js/jquery.cycle2.min.js"></script>




<script>

    $(document).ready(function () {
        $(".add-to-cart").click(function () {
            var id = $(this).attr("data-id");
            $.post("/cart/add/"+id, {}, function (data) {
                $("#cart-count").html('('+ data + ')');
            });
            return false;
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.carusel').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 2000
        });
    });
</script>
<script>
$(document).ready(function () {
    // Скроем все элементы, которые должны быть скрыты
    $(".spoiler-body").hide();
// При клике на элементе с классом spoiler-title
// показываем или скрываем элемент, который идёт за ним
    $(".spoiler-title").click(function(){
        $(this).next().toggle();
    });

})
</script>


</body>
</html>