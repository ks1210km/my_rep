<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Каталог</h2>
                    <div class="panel-group category-products">
                        <?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a
                                                href="/category/<?php echo $categoryItem['id'] ?>"><?php echo $categoryItem['name']; ?></a>
                                    </h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="view-product">
                          <img src="<?php echo Product::getImage($product['id']); ?>" alt=""/>
<!--                                <img src="//upload/images/products/--><?//=$product['id'];?><!--" alt=""/>-->
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="product-information"><!--/product-information-->
                                <?php if($product['is_new']) : ?>
                                <img src="/template/images/product-details/new.jpg" class="newarrival" alt=""/>
                                <?php endif;?>
                                <h2><?= $product['name']; ?></h2>
                                <p>Код товару: <?= $product['code']; ?></p>
                                <span>
                                            <span>US $<?= $product['price']; ?></span>
                                          <div> <a href="#"  data-id="<?=$product['id'];?>"
                                                   class="btn btn-default add-to-cart" >
                                                <i class="fa fa-shopping-cart"></i>В корзину </a></div>
                                        </span>
                                <p><b>Наличие:</b><?php if ($product['availability']) {
                                        echo ' На складі';
                                    } else {
                                        echo ' Немає в наявності';
                                    } ?></p>
                                <p><b>Стан:</b> Нове</p>
                                <p><b>Виробник:</b> <?= $product['brand']; ?></p>
                            </div><!--/product-information-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5>Опис товару</h5>
                            <?= $product['description'];?>
                        </div>
                    </div>
                </div><!--/product-details-->

            </div>
        </div>
    </div>
</section>


<br/>
<br/>

<?php include ROOT . '/views/layouts/footer.php'; ?>
